## Read Me

## Database

1- Database tables need to be created using sql schema, which is available under path: data/sigmavir_samknowstest.sql

2- Update database settings in classes/class.database.php

## Running scripts

1- To pre-process and aggregate data, run php file: aggregate.php. Change file path/name in php code if there is a need to run it for another json file. Or alternatively, replace existing file under path: data/testdata.json

2- To lookup certain unit_id, metric, date and hour, run php file: lookup.php (Modify input in php file as required)