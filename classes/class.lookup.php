<?php
/*
* Class to lookup aggregated data
*/
class Lookup {
	
	// Constructor
	public function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}
	
	/*
	*
	* process_request() to lookup the aggregated benchmark data from mysql database
	* 
	* @param unit_id, integer containing integer id, e.g. '1'
	* @param metric, string containing metric name, eg. 'download'
	* @param date, string containing date, yyyy-mm-dd
	* @param hour, integer containing hour, 0 - 23
	*
	*/
	public function process_request($unit_id, $metric, $date, $hour)
	{
		$sql_query1 = "SELECT a.id, a.unit_id, a.sample_size, a.mean, a.median, a.minimum, a.maximum, b.metric 
		FROM `sk_aggregated_benchmarks` a, `sk_metrics` b 
		WHERE a.unit_id='".$unit_id."' and a.date='".$date."' and a.hour='".$hour."' and a.metric_id=b.id and b.metric='".$metric."'";
		$rs1 = $this->mysqli->query($sql_query1) or die($this->mysqli->error." - Aggregation > store_units > q1");
		if($rs1->num_rows==0) {
			return false;
		}
		else {
			$row1 = $rs1->fetch_array(MYSQLI_ASSOC);
			return $row1;
		}
	}
}

?>