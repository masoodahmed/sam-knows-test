<?php
/*
* Class to process json data to store in mysql
*/
class Aggregation {
	
	// Constructor
	public function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}
	
	/*
	*
	* process_json() to process the json file and store in mysql database
	* 
	* @param file, string containing json file path
	*
	*/
	public function process_json($file)
	{
		$data = @file_get_contents($file);
		$data_points = json_decode($data, 1);
		foreach($data_points as $units) {
			$unit_id = $units['unit_id'];
			$unit_id = $this->store_units($unit_id);
			foreach($units['metrics'] as $metric=>$benchmarks) {
				$cnt = 0;
				echo "<br>\nUnit: ".$unit_id." - Metric: ".$metric."<br>\n";
				// insert metric if not already existed
				$metric_id = $this->store_metrics($metric);
				foreach($benchmarks as $benchmark) {
					$cnt++;
					echo "."; flush(); if(($cnt%100)==0) echo "<br>\n";
					$this->store_raw_benchmarks(array('unit_id'=>$unit_id, 'metric_id'=>$metric_id, 
					'value'=>$benchmark['value'], 'timestamp'=>$benchmark['timestamp']));
				}
				
				// aggregating required information from raw data points
				$this->aggregate_raw_data($unit_id, $metric_id, $benchmarks);
			}
		}
	}
	
	/*
	*
	* aggregate_raw_data() to process raw data points to get and store required information in db
	* 
	* @param unit_id, integer containing unit_id
	* @param metric_id, integer containing metric_id
	* @param unit_id, integer containing unit_id
	*
	*/
	private function aggregate_raw_data($unit_id, $metric_id, $benchmarks)
	{
		$data_points_array = array();
		foreach($benchmarks as $benchmark) {
			$date = date("Y-m-d",strtotime($benchmark['timestamp']));
			$hour = date("G",strtotime($benchmark['timestamp']));
			$data_points_all[$date][$hour][] = $benchmark['value'];
		}
		
		foreach($data_points_all as $date=>$data_points_hour) {
			foreach($data_points_hour as $hour=>$data_points) {
				$mean = $this->mmmms($data_points, 'mean');
				$median = $this->mmmms($data_points, 'median');
				$min = min($data_points);
				$max = max($data_points);
				$samplesize = count($data_points);
				
				$this->store_aggregated_benchmarks(array('unit_id'=>$unit_id, 
					'metric_id'=>$metric_id, 'mean'=>$mean, 'median'=>$median, 
					'min'=>$min, 'max'=>$max, 'samplesize'=>$samplesize, 
					'date'=>$date, 'hour'=>$hour));
			}
		}
	}
	
	/*
	*
	* store_units() to store unit_id if not already in db
	* 
	* @param unit_id, integer containing unit_id
	* @return unit_id, integer
	*
	*/
	private function store_units($unit_id)
	{
		$sql_query1 = "SELECT unit_id FROM `sk_units` WHERE unit_id = '".$unit_id."'";
		$rs1 = $this->mysqli->query($sql_query1) or die($this->mysqli->error." - Aggregation > store_units > q1");
		if($rs1->num_rows==0)
		{
			$sql_query2 = "INSERT IGNORE INTO sk_units (unit_id) VALUES('".$unit_id."')";
			$rs2 = $this->mysqli->query($sql_query2) or die($this->mysqli->error." - Aggregation > store_units > q2");
			
			$unit_id = $this->mysqli->insert_id;
		} 
		else
		{
			$row1 = $rs1->fetch_array(MYSQLI_ASSOC);
			$unit_id = $row1['unit_id'];
		}
		
		return $unit_id;
	}
	
	/*
	*
	* store_metrics() to store metric if not already in db
	* 
	* @param metric, string containing metric name
	* @return metric_id, integer
	*
	*/
	private function store_metrics($metric)
	{
		$sql_query1 = "SELECT id FROM `sk_metrics` WHERE metric = '".$this->mysqli->real_escape_string($metric)."'";
		$rs1 = $this->mysqli->query($sql_query1) or die($this->mysqli->error." - Aggregation > store_metrics > q1");
		if($rs1->num_rows==0)
		{
			$sql_query2 = "INSERT IGNORE INTO sk_metrics (metric) VALUES('".$this->mysqli->real_escape_string($metric)."')";
			$rs2 = $this->mysqli->query($sql_query2) or die($this->mysqli->error." - Aggregation > store_metrics > q2");
			
			$metric_id = $this->mysqli->insert_id;
		} 
		else
		{
			$row1 = $rs1->fetch_array(MYSQLI_ASSOC);
			$metric_id = $row1['id'];
		}
		
		return $metric_id;
	}
	
	/*
	*
	* store_raw_benchmarks() to store benchmark data point if not already in db
	* 
	* @param bm_datapoint, array containing data point
	*
	*/
	private function store_raw_benchmarks($bm_dpoints)
	{
		$sql_query1 = "INSERT IGNORE INTO sk_raw_benchmarks (unit_id, metric_id, value, timestamp) 
		VALUES('".$bm_dpoints['unit_id']."', '".$bm_dpoints['metric_id']."', '".$bm_dpoints['value']."', '".$bm_dpoints['timestamp']."')";
		$rs1 = $this->mysqli->query($sql_query1) or die($this->mysqli->error." - Aggregation > store_raw_benchmarks > q2");
	}
	
	/*
	*
	* store_aggregated_benchmarks() to store benchmark data point if not already in db
	* 
	* @param bm_datapoint, array containing data point
	*
	*/
	private function store_aggregated_benchmarks($agg_dpoints)
	{
		$sql_query1 = "INSERT IGNORE INTO sk_aggregated_benchmarks (unit_id, metric_id, sample_size, mean, median, minimum, maximum, date, hour) 
		VALUES('".$agg_dpoints['unit_id']."', '".$agg_dpoints['metric_id']."', '".$agg_dpoints['samplesize']."', 
		'".$agg_dpoints['mean']."', '".$agg_dpoints['median']."', '".$agg_dpoints['min']."', 
		'".$agg_dpoints['max']."', '".$agg_dpoints['date']."', '".$agg_dpoints['hour']."')";
		$rs1 = $this->mysqli->query($sql_query1) or die($this->mysqli->error." - Aggregation > store_raw_benchmarks > q2");
	}
	
	/*
	*
	* mmmms() to do calculation (mean/median/min/max/samplesize)
	* 
	* @param array, array containing integer values
	* @param output, string containing calcultation type
	*
	*/
	private function mmmms($array, $output = 'mean'){
	
		if(!is_array($array)){
			return FALSE;
		}else{
			switch($output){
				case 'mean':
					$count = count($array);
					$sum = array_sum($array);
					$total = $sum / $count;
				break;
				case 'median':
					rsort($array);
					$count = count($array);
					if(($count%2)==0) {
						$middle = round(count($array) / 2);
						$total = ($array[$middle-1]+$array[$middle])/2;
					}
					else {
						$middle = round(count($array) / 2);
						$total = $array[$middle-1];
					}
				break;
			}
			return $total;
		}
	}
	
}
?>