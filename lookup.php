<?php 
set_time_limit(0);
ini_set('memory_limit','6024M');

include 'classes/class.database.php';
include 'classes/class.lookup.php';

$db = Database::getInstance();
$mysqli = $db->getConnection();
$obj_lup = new Lookup($mysqli);

echo "<font color='#009900' face='Verdana' size='1'>";
echo "===============================================================================================<br>\n";
echo "Server Time: ".date("Y-m-d H:i:s")."<br>\n";
echo "Script: Data Analysis Script<br>\n";
echo "===============================================================================================<br>\n";
flush();

// input variables, change here for testing
$unit_id = '1';
$metric = 'download';
$date = '2017-02-15';
$hour = '21';

$report = $obj_lup->process_request($unit_id, $metric, $date, $hour);

echo "<b>Input</b><br>\n";
echo "unit_id: $unit_id<br>\n";
echo "metric: $metric<br>\n";
echo "date: $date<br>\n";
echo "hour: $hour<br>\n";
echo "----------------------------------------<br>\n<br>\n";
echo "<b>Output</b><br>\n";
if($report) {
echo "<pre>";
print_r($report);
echo "</pre>";
} else { echo "Invalid input or no results found...<br>\n"; }
$mysqli->close();

echo "<br>\n";
echo "===============================================================================================<br>\n";
echo "</font>";
flush();
?>