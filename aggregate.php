<?php 
set_time_limit(0);
ini_set('memory_limit','6024M');

include 'classes/class.database.php';
include 'classes/class.aggregation.php';

$db = Database::getInstance();
$mysqli = $db->getConnection();
$obj_agg = new Aggregation($mysqli);

echo "<font color='#009900' face='Verdana' size='1'>";
echo "===============================================================================================<br>\n";
echo "Server Time: ".date("Y-m-d H:i:s")."<br>\n";
echo "Script: Data Aggregation Script<br>\n";
echo "===============================================================================================<br>\n";
flush();

$obj_agg->process_json('data/testdata.json');

$mysqli->close();

echo "<br>\n";
echo "===============================================================================================<br>\n";
echo "</font>";
flush();
?>